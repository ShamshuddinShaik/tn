//
//  HWLoginViewModel.swift
//  HelloWorld
//
//  Created by Suneelahammad Shaik on 11/21/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    func validateLogin(with userName: String, password: String, completion: @escaping ((Bool, String) -> Void)) {
        
        ///Make url string with username and password
        
        DataManager.shared.validateLogin(with: "String") { (response, errorMessage) in
            //guard let weakSelf = self else { return }
            debugPrint(response)
            if errorMessage.isEmpty {
                return completion(true, "")
            } else {
                return completion(false, errorMessage)
            }
        }
    }
}
