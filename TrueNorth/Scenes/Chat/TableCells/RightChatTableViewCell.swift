//
//  RightChatTableViewCell.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/27/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class RightChatTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        message.clipsToBounds = true
        message.layer.cornerRadius = 5.0
        message.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
