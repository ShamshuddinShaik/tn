//
//  ViewController.swift
//  HelloWorld
//
//  Created by Suneelahammad Shaik on 11/21/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    ///View Model
    var viewModel = LoginViewModel()
    
    /// Keyboard variables
    var keyboardFrame: CGRect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerKeyBoardNotifications()
    }
    
    func registerKeyBoardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    //MARK: - IBActions
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard Reachability.isConnectedToNetwork() else {
            /// Show No Internet alert
            debugPrint("No Internet")
            return
        }
        
        let activityIndicator = ActivityIndicator(view:view, navigationController:nil,tabBarController: nil)
        activityIndicator.showActivityIndicator(with: "Logging in...")
        
        viewModel.validateLogin(with: "USerName", password: "Password") { (success, errorMessage) in
            guard success else  {
                /// Failure - Show alert
                debugPrint("Login Failure")
                activityIndicator.stopActivityIndicator()
                return
            }
            
            /// Success - Land on Home screen
            debugPrint("Login Success")
            activityIndicator.stopActivityIndicator()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        
        keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y == 0, let frame = keyboardFrame {
            self.view.frame.origin.y -= frame.height/2
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0, let frame = keyboardFrame {
            self.view.frame.origin.y += frame.height/2
        }
    }
}

//MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

