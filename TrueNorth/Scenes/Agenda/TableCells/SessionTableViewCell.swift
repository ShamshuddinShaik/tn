//
//  SessionTableViewCell.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/29/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class SessionTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var questionButton: UIButton!
    
    @IBOutlet weak var sessionName: UILabel!
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var sessionTime: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var liveSessionView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.addShadowEffect()
        // Question button
        profileButton.roundedCorners()
        questionButton.roundedCorners()
        questionButton.isEnabled = false
        questionButton.alpha = 0.0
        
        // Profile ImageView
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2.0
        profileImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with session:Sessions) {
        if session.isSessionRunning {
            liveSessionView.backgroundColor = UIColor(red: 30.0/255.0, green: 196.0/255.0, blue: 141.0/255.0, alpha: 1.0)
            questionButton.isEnabled = session.isSessionRunning
            questionButton.alpha = 1.0
        }
        sessionName.text = session.sessionName
    }
}
