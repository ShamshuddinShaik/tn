//
//  HomeViewViewController.swift
//  TruNorth
//
//  Created by Suneelahammad Shaik on 11/25/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

enum CellTypes {
    case Header
    case Event
    case Agenda
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var venueDetailsView: UIView!
    
    @IBOutlet weak var eventProgressButton: UIButton!
    
    
    @IBOutlet weak var agendaView: UIView!
    @IBOutlet weak var speakerView: UIView!
    @IBOutlet weak var attendeesView: UIView!
    @IBOutlet weak var wallView: UIView!
    
    
    var tapGesture = UITapGestureRecognizer()
    
    var viewModel = HomeViewModel()
    
    var cellTypes: [CellTypes] = [.Header, .Event, .Agenda]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = true
    }
    
    func configureView() {
        venueDetailsView.addShadowEffect()
        agendaView.addShadowEffect()
        speakerView.addShadowEffect()
        attendeesView.addShadowEffect()
        wallView.addShadowEffect()
        eventProgressButton.backgroundColor = UIColor(red: 30.0/255.0, green: 196.0/255.0, blue: 141.0/255.0, alpha: 1.0)
        eventProgressButton.addShadowEffect()
    }
    
    //MARK: - IBActions
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag

        switch tag {
        case 100: showAgenda()
        case 101: showSpeakers()
        case 102: print("Attendees")
        case 103: print("Wall")
        case .none:break
        case .some(_):break
        }
    }
    
    func showAgenda() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let agendaVC  = storyboard.instantiateViewController(withIdentifier: "AgendaViewController") as? AgendaViewController {
            navigationController?.navigationBar.isHidden = false
            navigationController?.pushViewController(agendaVC, animated: true)
        }
    }
    
    func showSpeakers() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let chatVC  = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            navigationController?.navigationBar.isHidden = false
            navigationController?.pushViewController(chatVC, animated: true)
        }
    }
    
    func showAttendees() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let agendaVC  = storyboard.instantiateViewController(withIdentifier: "AgendaViewController") as? AgendaViewController {
            navigationController?.navigationBar.isHidden = false
            navigationController?.pushViewController(agendaVC, animated: true)
        }
    }
    
    func showWall() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let agendaVC  = storyboard.instantiateViewController(withIdentifier: "AgendaViewController") as? AgendaViewController {
            navigationController?.navigationBar.isHidden = false
            navigationController?.pushViewController(agendaVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellType = cellTypes[indexPath.row]
        
        switch cellType {
        case .Header:
            if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.className, for: indexPath) as? HeaderTableViewCell {
                return cell
            }
        case .Event:
            if let cell = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.className, for: indexPath) as? EventTableViewCell {
                return cell
            }
        case .Agenda:
            if let cell = tableView.dequeueReusableCell(withIdentifier: AgendaTableViewCell.className, for: indexPath) as? AgendaTableViewCell {
                return cell
            }
        }
        return UITableViewCell()
    }
}

//MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
