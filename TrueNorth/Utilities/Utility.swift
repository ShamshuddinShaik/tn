//
//  Utility.swift
//  TruNorth
//
//  Created by Suneelahammad Shaik on 11/25/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct AppDetails {
        static let AppName = "TrueNorth"
        static let ok = "Ok"
    }
    
    struct NetworkErrors {
        static let noDataAvailable = "No data available"
    }
    
    struct DateFormatters {
        static let dateFormatter = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        static let dateFormatter = "HH:mm a"
        //        static let dateFormatter = "EEE"
        static let timeUTC = "UTC"
        static let dateFormaterLocale = "en_US_POSIX"
    }
}

class Utility: NSObject {
    
    class var dateFormatter : DateFormatter {
        struct Static {
            static let instance: DateFormatter = {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = Constants.DateFormatters.dateFormatter
                return dateFormatter
            }()
        }
        return Static.instance
    }
    
    class func dateToString(with formatter: String) -> String {
        dateFormatter.dateFormat = formatter
        return dateFormatter.string(from: Date())
        
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    ///To make part of text bold
    class func combine(bold text: String, with normalText: String) -> NSMutableAttributedString {
        let attr = NSMutableAttributedString(string: "\(text)\(normalText)")
        attr.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 26), range: NSMakeRange(0, text.count))
        return attr
    }
    
    class func convertToCurrentTimeZone(inputDateString:String, formatterString:String, outputFormat:String) -> String {
        // create dateFormatter with UTC time format
        dateFormatter.dateFormat = formatterString
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateFormatters.timeUTC)
        dateFormatter.locale = Locale(identifier: Constants.DateFormatters.dateFormaterLocale)
        let date = dateFormatter.date(from: inputDateString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = outputFormat
        dateFormatter.timeZone = TimeZone.current
        let resultTime = dateFormatter.string(from: date!)
        return resultTime
    }
    
    class func createNoDataLabel(tableView: UITableView, text: String) -> UILabel {
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text          = text //Constants.NetworkErrors.noDataAvailable
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        noDataLabel.tag = 100
        return noDataLabel
    }
    
    class func createAlertController(message: String) -> (UIAlertController) {
        let alertController = UIAlertController(title: Constants.AppDetails.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: Constants.AppDetails.ok, style: .default, handler: nil))
        return alertController
    }
}

struct ActivityIndicator {
    
    let viewForActivityIndicator = UIView()
    let view: UIView
    let navigationController: UINavigationController?
    let tabBarController: UITabBarController?
    let activityIndicatorView = UIActivityIndicatorView()
    let loadingTextLabel = UILabel()
    
    var navigationBarHeight: CGFloat { return navigationController?.navigationBar.frame.size.height ?? 0.0 }
    var tabBarHeight: CGFloat { return tabBarController?.tabBar.frame.height ?? 0.0 }
    
    func showActivityIndicator(with text: String) {
        viewForActivityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        viewForActivityIndicator.backgroundColor = UIColor.white
        view.addSubview(viewForActivityIndicator)
        
        activityIndicatorView.center = CGPoint(x: self.view.frame.size.width / 2.0, y: (self.view.frame.size.height - tabBarHeight - navigationBarHeight) / 2.0)
        
        loadingTextLabel.textColor = UIColor.black
        loadingTextLabel.text = text
        loadingTextLabel.font = UIFont.systemFont(ofSize: 17.0)
        loadingTextLabel.sizeToFit()
        loadingTextLabel.center = CGPoint(x: activityIndicatorView.center.x, y: activityIndicatorView.center.y + 30)
        viewForActivityIndicator.addSubview(loadingTextLabel)
        
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.style = .gray
        viewForActivityIndicator.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    func stopActivityIndicator() {
        viewForActivityIndicator.removeFromSuperview()
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
}

class BottomView: UIView {
    
    var showGray = true
    
    override func draw(_ rect: CGRect) {
        if showGray {
            self.setNeedsDisplay()
            self.backgroundColor = .white
            self.fillColor(with: UIColor(red: 21.0/255.0, green: 112.0/255.0, blue: 237.0/255.0, alpha: 1.0), height: self.frame.height/2.8)
        }
    }
    
    func fillColor(with color: UIColor, height: CGFloat) {
        let topRect = CGRect(x:0, y:0, width : self.bounds.width, height: height);
        color.setFill()
        UIRectFill(topRect)
    }
}
