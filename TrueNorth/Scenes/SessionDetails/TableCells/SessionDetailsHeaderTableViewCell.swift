//
//  SessionDetailsHeaderTableViewCell.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 12/5/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class SessionDetailsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var questionButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var textViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        questionButton.roundedCorners()
        contentView.addShadowEffect()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell() {
        textViewHeightConstraint.constant = self.textView.contentSize.height
    }
}
