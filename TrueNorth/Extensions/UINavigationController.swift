//
//  UINavigationController.swift
//  Lutetia
//
//  Created by Suneelahammad Shaik on 11/11/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func popTo(viewController: AnyClass) {
        for controller in self.viewControllers as Array {
            if controller.isKind(of: viewController) {
                self.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
}
