//
//  ChatHeaderView.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/28/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class ChatHeaderView: UIView {
    
    @IBOutlet weak var title: UILabel!
    
}
