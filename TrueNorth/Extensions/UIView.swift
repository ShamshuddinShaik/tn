//
//  UIView.swift
//  Lutetia
//
//  Created by Suneelahammad Shaik on 11/14/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addShadowEffect() {
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2.0
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.cornerRadius = 3.0
    }
    
    
    func setGradientBackground() {
        
        let colorTop = UIColor.blue.cgColor
        let colorBottom =  UIColor.white.cgColor //UIColor(red: 208.0/255.0, green: 181.0/255.0, blue: 168.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
