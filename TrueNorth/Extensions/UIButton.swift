//
//  UIButton.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 12/5/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func roundedCorners() {
        let color = UIColor(red: 21.0/255.0, green: 112.0/255.0, blue: 237.0/255.0, alpha: 1.0)
        self.layer.borderColor = color.cgColor
        self.titleLabel?.textColor = color
        self.layer.cornerRadius = 15.0
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = true
    }
}
