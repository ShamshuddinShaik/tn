//
//  AgendaViewController.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/29/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

struct Sessions {
    var isSessionRunning = false
    var sessionName = ""
}

class AgendaViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    /// Day 1
    @IBOutlet weak var dayOneView: UIView!
    /// Day 2
    @IBOutlet weak var dayTwoView: UIView!
    
    var sessions: [Sessions] = []
    
    static let breakSesionName = "Break"
    var indexOfRunningSession: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Agenda"
        /// To make Day 1 is in selected state
        dayOnePressed()
        filteredSessions()
    }
    
    //MARK: - IBActions
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag
        switch tag {
        case 100: dayOnePressed()
        case 101: dayTwoPressed()
        case .none: break
        case .some(_):break
        }
    }
    
    private func dayOnePressed() {
        dayOneView.isUserInteractionEnabled = false
        dayOneView.alpha = 1.0
        dayTwoView.isUserInteractionEnabled = true
        dayTwoView.alpha = 0.5
    }
    
    private func dayTwoPressed() {
        dayOneView.isUserInteractionEnabled = true
        dayOneView.alpha = 0.5
        dayTwoView.isUserInteractionEnabled = false
        dayTwoView.alpha = 1.0
    }
    
    func filteredSessions() {
        
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session1"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session2"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session3"))
        sessions.append(Sessions(isSessionRunning: true, sessionName: "Session4"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session5"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session6"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session7"))
        sessions.append(Sessions(isSessionRunning: false, sessionName: "Session8"))
        
        /// Add break session cell here after running session cell
        indexOfRunningSession = sessions.index { $0.isSessionRunning }
        if let index = indexOfRunningSession {
            sessions.insert(Sessions(isSessionRunning: true, sessionName: AgendaViewController.breakSesionName), at: index + 1)
        }
        
        /// Show message on no data
        guard sessions.count > 0 else {
            tableView.setEmptyMessage("No Sessions Found!")
            return
        }
        
        /// To show running session on top
        DispatchQueue.main.async { [weak self] in
            guard let weakSelf = self else { return }
            
            if let index = weakSelf.indexOfRunningSession {
                weakSelf.tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: false)
            }
        }
    }
}

//MARK: - UITableViewDataSource
extension AgendaViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sessions.count > 0 ? sessions.count : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let session = sessions[indexPath.section]
        switch session.isSessionRunning {
        case true:
            if session.sessionName == AgendaViewController.breakSesionName, let cell = tableView.dequeueReusableCell(withIdentifier: BreakSessionTableViewCell.className, for: indexPath) as? BreakSessionTableViewCell {
                return cell
            }
            fallthrough
        case false:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SessionTableViewCell.className, for: indexPath) as? SessionTableViewCell {
                cell.configure(with: session)
                return cell
            }
        }
        return UITableViewCell()
    }
}

//MARK: - UITableViewDelegate
extension AgendaViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layer.masksToBounds = true
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}

/*
 
 @IBOutlet weak var firstButton: UIButton!
 @IBOutlet weak var secondButton: UIButton!
 
 // First button
 let dayOneText = "Day 1"
 let firstFullText = "\(dayOneText) \nDec -21"
 configureHeaderButtons(for: firstButton, with: dayOneText, and: firstFullText)
 
 // Second button
 let dayTwoText = "Day 2"
 let secondFullText = "\(dayTwoText) \nDec -22"
 configureHeaderButtons(for: secondButton, with: dayTwoText, and: secondFullText)
 
 @IBAction func firstButtonClicked(_ sender: UIButton) {
 print("First")
 firstButton.isSelected = true
 firstButton.isEnabled = false
 secondButton.isEnabled = true
 secondButton.isSelected = false
 }
 
 @IBAction func secondButtonClicked(_ sender: UIButton) {
 print("Second")
 firstButton.isSelected = false
 firstButton.isEnabled = true
 secondButton.isEnabled = false
 secondButton.isSelected = true
 }
 
 func configureHeaderButtons(for button: UIButton, with boldText: String, and fullText: String) {
 
 // .Normal
 let myNormalAttributedTitle = NSMutableAttributedString(string: fullText)
 myNormalAttributedTitle.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 26), range: NSMakeRange(0, boldText.count))
 myNormalAttributedTitle.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.lightGray], range: NSMakeRange(0, fullText.count))
 button.setAttributedTitle(myNormalAttributedTitle, for: .normal)
 
 // .Selected
 let mySelectedAttributedTitle = NSMutableAttributedString(string: fullText)
 mySelectedAttributedTitle.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 26), range: NSMakeRange(0, boldText.count))
 
 mySelectedAttributedTitle.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], range: NSMakeRange(0, fullText.count))
 button.setAttributedTitle(mySelectedAttributedTitle, for: .selected)
 }*/
