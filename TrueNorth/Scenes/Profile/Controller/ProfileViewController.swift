//
//  ProfileViewController.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 12/5/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var mailImageView: UIImageView!
    @IBOutlet weak var chatImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Profile"
        setupUI()
    }
    
    func setupUI() {
        configureImageView(imageView: profileImageView)
        configureImageView(imageView: mailImageView)
        configureImageView(imageView: chatImageView)
    }
    
    func configureImageView(imageView: UIImageView) {
        imageView.layer.cornerRadius = imageView.frame.size.width/2
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.masksToBounds = true
    }
}
