//
//  ChatViewController.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/26/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit
import SwiftR

class ChatViewController: UIViewController {
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatBottomView: UIView!
    
    let placeHolderText = "Write a message here..."
    
    var userSwitch = false
    var messages = ["Message1", "Message2", "Message3"]
    var tempMessages:[String] = []
    var sections = [0,1,2,3,4,5,6,7,8]
    var keyboardFrame: CGRect?
    
    var chatHub: Hub!
    var connection: SignalR!
    //    var name: String!
    var name = "Suneel"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Chat"
        setupUI()

        tempMessages = messages
        sendButton.isHidden = true
        
        registerKeyBoardNotifications()
        workingSample1()
    }
    
    func setupUI() {
        messageTextView.autocorrectionType = .no
        messageTextView.text = placeHolderText
        messageTextView.textColor = .darkGray
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        if let hub = chatHub, let message = messageTextView.text {
            do {
                messageTextView.text = ""
                sendButton.isEnabled = false
                try hub.invoke("send", arguments: [name, message])
            } catch {
                print(error)
            }
        }
        
        // Send data
        //        connection.send("Persistent Connection Test")
        messageTextView.resignFirstResponder()
    }
    
    func registerKeyBoardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        
        keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y == 0, let frame = keyboardFrame {
            self.view.frame.origin.y -= frame.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0, let frame = keyboardFrame {
            self.view.frame.origin.y += frame.height
        }
    }
    
    func scrollToBottom(){
        tableView.scrollRectToVisible(CGRect(x: 0, y: self.tableView.contentSize.height - self.tableView.bounds.size.height, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height), animated: true)
    }
    
    func workingSample1() {
        connection = SignalR("http://swiftr.azurewebsites.net")
        connection.useWKWebView = true
        connection.signalRVersion = .v2_2_0
        
        chatHub = Hub("chatHub")
        chatHub.on("broadcastMessage") { [weak self] args in
            
            guard let weakSelf = self else { return }
            
            if let _ = args?[0] as? String, let message = args?[1] as? String {
                weakSelf.tempMessages.append("\(message)")
                
                //                weakSelf.tableView.reloadData()
                let indexPath = IndexPath(row: weakSelf.tempMessages.count - 1 , section: weakSelf.sections.count - 1)
                weakSelf.tableView.beginUpdates()
                weakSelf.tableView.insertRows(at: [indexPath], with: .middle)
                weakSelf.tableView.endUpdates()
                weakSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
        
        connection.addHub(chatHub)
        
        // SignalR events
        connection.starting = { [weak self] in
            self?.sendButton.isEnabled = false
        }
        
        connection.reconnecting = { [weak self] in
            self?.sendButton.isEnabled = false
        }
        
        connection.connected = { [weak self] in

            print("Connection ID: \(String(describing: self?.connection.connectionID))")
            UserDefaults.standard.set("\(String(describing: self?.connection.connectionID))", forKey: "UserID")
            self?.sendButton.isHidden = false
        }
        
        connection.reconnected = { [weak self] in
            UserDefaults.standard.set("\(String(describing: self?.connection.connectionID))", forKey: "UserID")
            self?.sendButton.isEnabled = true
        }
        
        connection.disconnected = { [weak self] in
            self?.sendButton.isEnabled = false
        }
        
        connection.connectionSlow = { print("Connection slow...") }
        
        connection.error = { [weak self] error in
            print("Error: \(String(describing: error))")
            
            // Here's an example of how to automatically reconnect after a timeout.
            //
            // For example, on the device, if the app is in the background long enough
            // for the SignalR connection to time out, you'll get disconnected/error
            // notifications when the app becomes active again.
            
            if let source = error?["source"] as? String, source == "TimeoutException" {
                print("Connection timed out. Restarting...")
                self?.connection.start()
            }
        }
        
        connection.start()
    }
    
    func workingSample2() {
        // Client
        connection = SignalR("http://swiftr.azurewebsites.net", connectionType: .persistent)
        connection.received = { data in
            print(data!)
        }
        connection.start()
    }
}

//MARK: - UITableViewDataSource
extension ChatViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sections.count - 1 {
            return tempMessages.count
        }
        
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            if let leftChatCell = tableView.dequeueReusableCell(withIdentifier: LeftChatTableViewCell.className, for: indexPath) as? LeftChatTableViewCell {
                leftChatCell.message.text = (indexPath.section == sections.count - 1) ? tempMessages[indexPath.row] : messages[indexPath.row]
                leftChatCell.time.text = Utility.dateToString(with: "h:mm a")
                return leftChatCell
            }
        } else {
            if let rightChatCell = tableView.dequeueReusableCell(withIdentifier: RightChatTableViewCell.className, for: indexPath) as? RightChatTableViewCell {
                rightChatCell.message.text = (indexPath.section == sections.count - 1) ? tempMessages[indexPath.row] : messages[indexPath.row]
                rightChatCell.time.text = Utility.dateToString(with: "h:mm a")
                return rightChatCell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Date().toRleativeDay(Date(), with: "MM-dd-yyyy")
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textAlignment = .center
            headerView.textLabel?.textColor = .darkGray
            let view = UIView(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height))
            view.backgroundColor = .clear
            headerView.backgroundView = view
        }
    }
}

//MARK: - UITableViewDelegate
extension ChatViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ChatViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        sendButton.isEnabled = (textView.text == "Write a message here..." ? false: true)
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeHolderText) {
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolderText
        }
        textView.resignFirstResponder()
    }
}
