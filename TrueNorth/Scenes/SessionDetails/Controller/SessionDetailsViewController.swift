//
//  SessionDetailsViewController.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 12/4/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class SessionDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Session Details"
    }
}

//MARK: - UITableViewDataSource
extension SessionDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: SessionDetailsHeaderTableViewCell.className, for: indexPath) as? SessionDetailsHeaderTableViewCell {
            cell.configureCell()
            return cell
        }
        
        return UITableViewCell()
    }
}

//MARK: - UITableViewDelegate
extension SessionDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layer.masksToBounds = true
    }
}

