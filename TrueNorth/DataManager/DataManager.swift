//
//  DataManager.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/26/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    func validateLogin(with urlString: String, completion: @escaping (_ result: [String:AnyObject], _ message:String) -> Void) {
        
        ServiceManager.shared.makeServiceCall(with: urlString) { (response, errorMessage) in
            
            if errorMessage != "" {
                completion([:], errorMessage)
            } else {
                completion(response, "")
            }
        }
    }
}

