//
//  ServiceManager.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/26/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager {
    
    static let shared = ServiceManager()
    

    //MARK: - POST
    public func initiateServiceCall(urlString: String, parameters:[String:Any], completion: @escaping (_ result: [String:AnyObject], _ message:String) -> Void) {
        
//        let headers: HTTPHeaders = [
//            AutherizationHeaders.authorizationHeaderName: AutherizationHeaders.authorization,
//            AutherizationHeaders.contentTypeHeaderName :AutherizationHeaders.contentType
//        ]
        
        var serviceUrl = URL(string: "serviceBaseUrl")
        serviceUrl = serviceUrl?.appendingPathComponent(urlString)
        
        Alamofire.request(serviceUrl!, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<600)
            .validate(contentType: ["(contentType)"]).responseJSON { (response:DataResponse<Any>) in
        
                debugPrint(response)
                self.handleResponse(response: response, completion: completion)
        }
    }
    
    //MARK: - GET
    public func makeServiceCall(with urlString: String, completion: @escaping (_ result: [String:AnyObject], _ message:String) -> Void) {
        
//        let headers: HTTPHeaders = [
//            AutherizationHeaders.authorizationHeaderName: AutherizationHeaders.authorization,
//            AutherizationHeaders.contentTypeHeaderName :AutherizationHeaders.contentType
//        ]
        
        var serviceUrl = URL(string: "serviceBaseUrl")
        serviceUrl = serviceUrl?.appendingPathComponent(urlString)
        
        Alamofire.request(serviceUrl!, headers: nil).validate(statusCode: 200..<600)
            .validate(contentType: ["contentType"])
            .responseJSON { response in
                
                debugPrint(response)
                self.handleResponse(response: response, completion: completion)
                
        }
    }
    
    //MARK: - Handle Response
    public func handleResponse(response: DataResponse<Any>, completion: @escaping (_ result: [String:AnyObject], _ message:String) -> Void) {
        
        switch(response.result) {
        case .success(_):
            if let statusCode = response.response?.statusCode, 200...299 ~= statusCode {
                guard (response.result.value != nil) else {
                    completion([:], "errorMessage")
                    return
                }
                guard let dict = response.result.value as? [String:AnyObject] else {
                    completion([:], "errorMessage")
                    return
                }
                completion(dict, "")
            } else {
                guard (response.result.value != nil) else { return }
                if let dict = (response.result.value as! NSDictionary) as? [String:AnyObject] {
                    if(dict["isSuccessStatus"] as! Int == 0) {
                        if let contentReultsString = dict["contentResult"] as? String {
                            completion([:], contentReultsString)
                            return
                        }
                        
                        let errorResponseList = dict["errorResponseMessageListKey"] as! [Any]
                        guard errorResponseList.count > 0 else { return }
                        if let errorResponseDict = errorResponseList[0] as? [String:AnyObject] {
                            if let errorMessage = errorResponseDict["errorMessageKey"] as? String {
                                completion([:], errorMessage)
                            } else {
                                completion([:], "troubleInConnectingServer")
                            }
                        }
                    }
                }
            }
            break
        case .failure(_):
            if let statusCode = response.response?.statusCode , 400...499 ~= statusCode {
                completion([:], "errorMessage")
            }
            else {
                completion([:], response.error!.localizedDescription)
            }
            break
        }
    }
}
