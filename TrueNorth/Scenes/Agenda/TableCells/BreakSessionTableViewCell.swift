//
//  BreakSessionTableViewCell.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 12/4/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class BreakSessionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var breakTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.addShadowEffect()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
