//
//  UIDate.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/28/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import Foundation

extension Date {
    
    var weekAgoDaysFromNow: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: -7, to: Date(), options: []) ?? Date()
    }

    /// To show a week days like Monday, Tuesday and then it shows you the custom formet of Dates
    func toRleativeDay(_ date: Date, with format: String) -> String {
        // Setup the relative formatter
        let relDF = DateFormatter()
        relDF.doesRelativeDateFormatting = true
        relDF.dateStyle = .long
        relDF.timeStyle = .none
        
        let absDF = DateFormatter()
        absDF.dateStyle = .long
        absDF.timeStyle = .none
        
        let rel = relDF.string(from: date)
        let abs = absDF.string(from: date)
        
        if rel == abs {
            let fullDF = DateFormatter()
            if date.isBetween(date: weekAgoDaysFromNow, andDate: Date()) {
                if let dayOfWeek = date.dayOfWeek() {
                    return dayOfWeek
                }
            }
            fullDF.dateFormat = format
            return fullDF.string(from: date)
        }
        return rel
    }
    
    func isBetween(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self).rawValue * self.compare(date2).rawValue >= 0
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}
