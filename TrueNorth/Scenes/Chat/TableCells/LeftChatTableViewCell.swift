//
//  LeftChatTableViewCell.swift
//  TrueNorth
//
//  Created by Suneelahammad Shaik on 11/27/18.
//  Copyright © 2018 Suneelahammad Shaik. All rights reserved.
//

import UIKit

class LeftChatTableViewCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        message.layer.cornerRadius = 5.0
        message.clipsToBounds = true
        message.adjustsFontSizeToFitWidth = true
        userImage.layer.cornerRadius = userImage.frame.width/2
        userImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
